/*
 * rnd_xorshift.h
 *
 *  Created on: Jan. 11, 2020
 *      Author: raschuh
 * 
 * A more effective implementation of a pseudorandom number generator (PRNG)
 * using a xorshift16 algorithm optimized for AVR. 
 */

#ifndef INCLUDE_RND_XORSHIFT_H_
#define INCLUDE_RND_XORSHIFT_H_

#include <stdint.h>

/*
    The PRNG must be initialized (seeded) first before using.
    The ADC must be powered on first before calling this function.

    Noises from the ADC will be used to generate a randomized seed.

    adc_pin: The ADC input pin to be used to collect noise.
*/
void rnd_xorshift_init(uint8_t adc_pin);

/*
    Generate a random 16-bit unsigned integer then return it.
*/
uint16_t rnd_xorshift();

#endif // INCLUDE_RND_XORSHIFT_H_
