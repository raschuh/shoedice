/*
 * tm1637.h
 *
 *  Created on: Jan. 9, 2020
 *      Author: raschuh
 * 
 * An easy to use library for a TM1637 based 7-segments LED display.
 */

#ifndef RAS_TM1637_SOFTWARE
#define RAS_TM1637_SOFTWARE

#include <stdint.h>
#include "common.h"

/*
	A driver struct to control a single TM1637 LED Driver based 7-Seg Display.

	It contains only critical information to correctly control a display.
	Do not modify the struct's members otherwise undefined behaviours might
	occur. The struct is made public so it is possible to control multiple
	displays using multiple driver structs.
*/
typedef struct {
	sfr_ptr port; 		// A pointer to the address of a GPIO port
	uint8_t clk;  		// A bitmask for CLK pin
	uint8_t dio;		// A bitmask for DIO pin
	uint8_t brightness; // Current brightness setting
} TM1637_Driver;

/*
	Initialize the driver struct. Must be called first before using the driver.

	driver: the driver struct to be initalized
	port: the address of a GPIO port, for example, &PORTA
	clk_pin: the pin number for clock
	dio_pin: the pin number for data
*/
void tm1637_init(TM1637_Driver *driver, sfr_ptr port, uint8_t clk_pin, uint8_t dio_pin);

/*
	Clear the 7-segs display.

	driver: the display to be cleared
*/
void tm1637_clear(TM1637_Driver *driver);

/*
	Set the display brightness and shutdown mode.

	driver: the display to be set
	brightness: brightness level, 0 as lowest to 7 as full
	on: 1 to turn the display on or 0 to turn it off
*/
void tm1637_brightness(TM1637_Driver *driver, uint8_t brightness, uint8_t on);

/*
	Display an array of segments values on the display where a segments value is a bitfield
	that toggle individual LED segments in a 7-seg digit.

	display: the destination display
	segments: a 4 elements maximum array of segments values
	pos: the starting position on the display where 0 is leftmost and 3 is rightmost
	length: the length of the array, must be 1 to 4
*/
void tm1637_display(TM1637_Driver *driver, uint8_t *segments, uint8_t pos, uint8_t length);

/*
	Convert a hex value to a segments value where a segments value is a bitfield
	that toggle individual LED segments in a 7-seg digit.

	hex_value: a hexidecimal value

	Returns: the converted value
*/
uint8_t tm1637_hex2seg(uint8_t hex_value);

#endif // RAS_TM1637_SOFTWARE
