/*
 * die.h
 *
 *  Created on: Jan. 14, 2020
 *      Author: raschuh
 * 
 * A DnD style dice rolling system capable of rolling up to four single
 * digit dice and up to two double digit dice.
 */

#ifndef SHOEDICE_DIE_H
#define SHOEDICE_DIE_H

#include <stdint.h>

/*
    Initialize the dice system before using it.
*/
void init_dice();

/*
    Get the current dice shape.

    Returns one of d4, d6, d8, d10, d12, or d20.
*/
uint8_t dice_shape_value();

/*
    Increment to the next dice shape.
*/
void change_dice_shape();

/*
    Roll a die then read its value.

    Calling this function will clear the buffer of the recently rolled dice.

    Returns the die value.
*/
uint8_t roll_dice_once();

/*
    Roll a die, read its value then append th e die to the buffer of
    recently rolled die.

    If the buffer is already full then it will clear the buffer.

    Returns the latest rolled die value.
*/
uint8_t roll_dice_append();

/*
    Roll all dice at once.
*/
void roll_all_dice();

/*
    Get the values of the dice stored in the buffer.

    dice_vals: the array to read the values into
*/
void get_dice_values(uint8_t *dice_vals);

#endif // SHOEDICE_DIE_H
