/*
 * common.h
 *
 *  Created on: June 9, 2020
 *      Author: raschuh
 * 
 * A set of helper macros for common AVR operations such as setting 
 * or clearing bits
 */

#ifndef INCLUDE_AVR_COMMON_H_
#define INCLUDE_AVR_COMMON_H_

#include <stdint.h>

#define ERASE(sfr) ((sfr) = 0)
#define ERASE_SET_BIT(sfr, bit) ((sfr) = (1 << (bit)))
#define ERASE_SET_BMASK(sfr, mask) ((sfr) = (mask))

#define SET_BIT(sfr, bit) ((sfr) |= (1 << (bit)))
#define SET_BMASK(sfr, mask) ((sfr) |= (mask))

#define CLR_BIT(sfr, bit) ((sfr) &= ~(1 << (bit)))
#define CLR_BMASK(sfr, mask) ((sfr) &= ~(mask))

#define READ_BIT(sfr, bit) ((sfr) & (1 << (bit)))

typedef volatile unsigned char * sfr_ptr;

#endif //INCLUDE_AVR_COMMON_H_