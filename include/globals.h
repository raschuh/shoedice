/*
 * globals.h
 *
 *  Created on: June 10, 2020
 *      Author: raschuh
 * 
 * Global variables and constants such as buffers to be used by ISRs goes here.
 */

#ifndef INCLUDE_SHOEDICE_GLOBALS_H_
#define INCLUDE_SHOEDICE_GLOBALS_H_

#include <stdint.h>
#include <avr/io.h>

// ***** Pin definitions *****

// Dice shape mode button
#define  MODE_BTN_PIN	PA1

// Roll button
#define  ROLL_BTN_PIN   PA2

// TM1637 TWI DIO pin
#define  DIO_PIN		PB0

// TM1637 TWI CLK pin
#define  CLK_PIN		PB1


extern volatile uint16_t millis_clock;      // System-wide millisecond clock
extern volatile uint8_t mode_btn_register;  // Debounce shift register for the mode button
extern volatile uint8_t roll_btn_register;  // Debounce shift register for the roll button

#endif //INCLUDE_SHOEDICE_GLOBALS_H_