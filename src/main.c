/*
 * main.c
 *
 *  Created on: Jan. 13, 2020
 *      Author: raschuh
 */

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include "tm1637.h"
#include "rnd_xorshift.h"
#include "die.h"
#include "globals.h"

// Roll button hold timer - the length of hold time is used to determine which
// rolling action to take. Short press to roll and append, and long press
// to roll only once
uint16_t press_timer = 0;

// Wait timer before going into power down mode
uint16_t sleep_timer = 0;

/*
	Check if the target time has passed by comparing against the system clock and
	time delta.

	t: timer object in milliseconds
	dt: time delta in milliseconds

	Returns true if the time delta has elapsed

	TODO: Fix this poorly written doc
*/
uint8_t has_elapsed(uint16_t t, uint16_t dt) {
	uint16_t ms_clk = millis_clock;
	if (t > ms_clk) {
		ms_clk = ms_clk + (UINT16_MAX - t);
		t = 0;
	}

	return ((ms_clk - t) > dt) ? 1 : 0;
}

/*
	An utility function that ties the TM1637 driver and the dice module
	together to display the dice on the LED display.

	driver: a display to show the rolled dice
*/	
void display_dice(TM1637_Driver *driver) {
	uint8_t dice[4];
	get_dice_values(dice);

	uint8_t segs[4];

	// Display up to 4 dice for single digit dice
	if (dice_shape_value() < 10) {
		segs[0] = (dice[3] > 0) ? tm1637_hex2seg(dice[3]) : 0;
		segs[1] = (dice[2] > 0) ? tm1637_hex2seg(dice[2]) : 0;
		segs[2] = (dice[1] > 0) ? tm1637_hex2seg(dice[1]) : 0;
		segs[3] = (dice[0] > 0) ? tm1637_hex2seg(dice[0]) : 0;
	} else { 
		// Otherwise display up to 2 dice for double digit dice
		uint8_t num = dice[2];
		segs[0] = ((num / 10) > 0) ? tm1637_hex2seg(num / 10) : 0;
		segs[1] = (num > 0) ? tm1637_hex2seg(num % 10) | 0x80 : 0;
		num = dice[0];
		segs[2] = ((num / 10) > 0) ? tm1637_hex2seg(num / 10) : 0;
		segs[3] = tm1637_hex2seg(num % 10);
	}

	// First display a rolling animation
	for (uint8_t i = 0; i < 4; i++) {
		uint8_t vfx[1] = { rnd_xorshift() & 0x7F };
		tm1637_display(driver, vfx, i, 1);
		_delay_ms(75);
	}

	// Then display the rolled dice
	tm1637_display(driver, segs, 0, 4);
}

/*
	An utility function that ties the TM1637 driver and the dice module
	together to display the current die shape on the LED display.

	driver: a display to show the current die shape
*/	
void display_dice_shape(TM1637_Driver *driver) {
	uint8_t shape = dice_shape_value();
	uint8_t segs[3] = {
		tm1637_hex2seg(0xd),
		tm1637_hex2seg(shape / 10),
		tm1637_hex2seg(shape % 10)
	};
	tm1637_clear(driver);
	tm1637_display(driver, segs, 1, 3);
	_delay_ms(1000);
	tm1637_clear(driver);
}

int main() {
	// Seed the PRNG using noises from ADC7
	rnd_xorshift_init(PA7);

	// Setup the dice struct
	init_dice();

	// Config the TM1637 pins as output and the pushbutton pins as input with pullup
	DDRB = _BV(CLK_PIN) | _BV(DIO_PIN);
	PORTA = _BV(MODE_BTN_PIN) | _BV(ROLL_BTN_PIN);

	// Initialize the TM1637 then turn it on with the display blanked
	TM1637_Driver driver;
	tm1637_init(&driver, &PORTB, CLK_PIN, DIO_PIN);
	tm1637_brightness(&driver, 0x0, 0x1);
	tm1637_clear(&driver);

	// Config the Timer 0 to generate 1 ms ticks
	TCCR0A = (0x2 << WGM00); // CTC Mode
	TCCR0B = (0x3 << CS00); // 1/64 prescaler
	OCR0A = 15;
	TIMSK0 = _BV(OCIE0A); // Enable Compare Match Interrupt

	// Setup the interrupt
	PCMSK0 = _BV(ROLL_BTN_PIN);
	GIMSK = _BV(PCIE0); // Enable PCINT-A

	// Shut down the unnecessary modules to reduce power
	ADCSRA = 0;
	PRR = _BV(PRTIM1) | _BV(PRUSI) | _BV(PRADC);

	sei();

	display_dice_shape(&driver);

	while (1) {
		if (mode_btn_register == 0) {
			mode_btn_register = 0xFF;

			change_dice_shape();
			display_dice_shape(&driver);

			sleep_timer = millis_clock;
		}

		if (roll_btn_register == 0) {
			roll_btn_register = 0xFF;

			if (has_elapsed(press_timer, 1000)) {
				roll_dice_once();
			} else {
				roll_dice_append();
			}

			display_dice(&driver);

			press_timer = millis_clock;
			sleep_timer = millis_clock;
		}

		if (has_elapsed(sleep_timer, 15000)) {
			tm1637_brightness(&driver, 0x0, 0x0);
			tm1637_clear(&driver);

			set_sleep_mode(SLEEP_MODE_PWR_DOWN);
			sleep_mode();

			tm1637_brightness(&driver, 0x0, 0x1);
			display_dice_shape(&driver);
			sleep_timer = millis_clock;
		}
	}
}