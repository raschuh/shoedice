/*
 * tm1637.c
 *
 *  Created on: Jan. 9, 2020
 *      Author: raschuh
 * 
 * The implementation of the TM1637 Driver
 * 
 * NOTE: The TM1637 use a TWI-like serial interface, however not real TWI/I2C
 * as it doesn't have a slave address and send out data LSB first, not MSB. Hence,
 * it is not compatible with USI/I2C/etc therefore bit-banging is the only option.
 */

#include "tm1637.h"

#include <stdint.h>
#include <avr/sfr_defs.h>
#include <util/delay.h>
#include "common.h"

#define TD_TWI_US 5		// TWI clock hold time in microseconds
#define DELAY() (_delay_us(TD_TWI_US))		// Readable macro for TWI clock hold delay

// *************** Private helper functions ***************
// Bit-bang implementation of "TWI" master protocol
/*
	Emit the start condition
*/
static void _start(TM1637_Driver *driver);

/*
	Transmit a data byte as the master transmitter

	TODO: Add a handler for ACK/NACK bit status
*/
static void _write(TM1637_Driver *driver, uint8_t byte);

/*
	Emit the stop condition
*/
static void _stop(TM1637_Driver *driver);

// ********************************************************

void tm1637_init(TM1637_Driver *driver, sfr_ptr port, uint8_t clk_pin, uint8_t dio_pin) {
	driver->port = port;
	driver->clk = _BV(clk_pin);
	driver->dio = _BV(dio_pin);

	// Initialize the software TWI bus lines by driving both high
	SET_BMASK(*(driver->port), (driver->clk | driver->dio));
}

void tm1637_clear(TM1637_Driver *driver) {
	uint8_t blanks[] = { 0, 0, 0, 0 };
	tm1637_display(driver, blanks, 0, 4);
}

void tm1637_brightness(TM1637_Driver *driver, uint8_t brightness, uint8_t on) {
	// TM1637 brightness control register stores both brightness and shutdown mode
	// as 4 bits whereas the 4th bit is shutdown mode and the last 3 bits are
	// brightness levels
	driver->brightness = (brightness & 0x7) | ((on & 0x1) << 3);
}

void tm1637_display(TM1637_Driver *driver, uint8_t *segments, uint8_t pos, uint8_t length) {
	// Enter the data control and issue data write command
	_start(driver);
	_write(driver, 0x40);
	_stop(driver);

	// Enter the address control and write data byte(s) to each digits
	_start(driver);
	_write(driver, 0xC0 + (pos & 0x3));
	for (uint8_t seg = 0; seg < length; seg++) {
		_write(driver, segments[seg]);
	}
	_stop(driver);

	// Enter the display control and set brightness and shutdown mode
	_start(driver);
	_write(driver, 0x80 + (driver->brightness & 0xF));
	_stop(driver);
}

uint8_t tm1637_hex2seg(uint8_t hex_value) {
	static uint8_t HEX2SEGMENTS[] = {
		//XGFEDCBA
		0b00111111, // 0
		0b00000110, // 1
		0b01011011, // 2
		0b01001111, // 3
		0b01100110, // 4
		0b01101101, // 5
		0b01111101, // 6
		0b00000111, // 7
		0b01111111, // 8
		0b01101111, // 9
		0b01110111, // A
		0b01111100, // B
		0b00111001, // C
		0b01011110, // D
		0b01111001, // E
		0b01110001  // F
	};

	return HEX2SEGMENTS[hex_value & 0xF];
}

static void _start(TM1637_Driver *driver) {
	CLR_BMASK(*(driver->port), driver->dio);
	DELAY();
}

static void _write(TM1637_Driver *driver, uint8_t byte) {
	// Transmit the byte, LSB first and on the clock positive edge
	for (uint8_t bit = 0; bit < 8; bit++) {
		CLR_BMASK(*(driver->port), driver->clk);
		DELAY();
		(byte & 0x01) ? SET_BMASK(*(driver->port), driver->dio) : CLR_BMASK(*(driver->port), driver->dio);
		DELAY();
		SET_BMASK(*(driver->port), driver->clk);
		DELAY();
		byte >>= 1;
	}

	// ACK/NACK
	// TODO: It does nothing, just generate the 9th clock pulse, no handling
	CLR_BMASK(*(driver->port), driver->clk);
	DELAY();
	SET_BMASK(*(driver->port), driver->clk);
	DELAY();
	CLR_BMASK(*(driver->port), driver->clk);
	SET_BMASK(*(driver->port), driver->dio);
}

static void _stop(TM1637_Driver *driver) {
	CLR_BMASK(*(driver->port), driver->dio);
	DELAY();
	SET_BMASK(*(driver->port), driver->clk);
	DELAY();
	SET_BMASK(*(driver->port), driver->dio);
	DELAY();
}
