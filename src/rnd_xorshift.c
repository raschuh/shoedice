/*
 * rnd_xorshift.c
 *
 *  Created on: Jan. 11, 2020
 *      Author: raschuh
 * 
 * The implementation of the xorshift16 PRNG.
 */

#include "rnd_xorshift.h"

#include <avr/io.h>
#include "common.h"

// The algorithm requires two 16-bit states.
static uint16_t _state_a = 0;
static uint16_t _state_b = 0;

/*
	A helper function for the PRNG initialization stage.
	It will collect noises from the ADC then return a 16-bit seed.
*/
static uint16_t _get_rnd_seed();

void rnd_xorshift_init(uint8_t adc_pin) {
	adc_pin &= 0x7;
	ERASE_SET_BMASK(ADMUX, adc_pin);
	ERASE_SET_BMASK(ADCSRA, _BV(ADEN) | (0x6 << ADPS0)); // Enables the ADC and set the prescalar to 64
	_state_a = _get_rnd_seed();
	_state_b = _get_rnd_seed();
	CLR_BIT(ADCSRA, ADEN);
}

uint16_t rnd_xorshift() {
	/*
		The algorithm: 

		t = a xor (a << 5)
		a = b
		b = (b xor (b >> 1)) xor (t xor (t >> 3))

		where t is a temporary state and a and b are the states
	*/

	uint16_t t = (_state_a ^ (_state_a << 5));
	_state_a = _state_b; // a is the previous state
	_state_b = (_state_b ^ (_state_b >> 1)) ^ (t ^ (t >> 3));
	return _state_b; // b is the new state
}

static uint16_t _get_rnd_seed() {
	/*
		The LSB of an ADC value is the noisiest hence more random.
		So, read in the LSB then shift it into a seed 16 times to
		generate a 16-bit random seed.
	*/

	uint16_t seed = 0;

	for (uint8_t i = 0; i < 16; i++) {
		SET_BIT(ADCSRA, ADSC);
		loop_until_bit_is_clear(ADCSRA, ADSC);
		uint8_t adcl = ADCL;
		uint8_t adch = ADCH;
		seed = (adcl & 0x1) | (seed << 1);
	}

	return seed;
}
