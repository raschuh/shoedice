/*
 * die.c
 *
 *  Created on: Jan. 14, 2020
 *      Author: raschuh
 * 
 * The implementation of the dice rolling system. The rolling will be
 * powered by the xorshift16 PRNG engine.
 */

#include <stdint.h>

#include "die.h"
#include "rnd_xorshift.h"

/*
	The die rolling mechanism - get a random value from the xorshift
	PRNG then compute it into a die value.
*/
static uint8_t _roll();

/*
	A helper function to clear the dice buffer.
*/
static void _clear_dice_array();

// The dice system states
static struct {
	uint8_t die[4]; // The buffer of the recently rolled dice - up to 4 dice
	uint8_t digit_index; // The current dice buffer index position
	uint8_t die_shape[6]; // The enum of the six common DnD style dice shapes
	uint8_t shape_index; // The current dice shape index
} dice;

void init_dice() {
	dice.digit_index = 0;
	dice.shape_index = 1; // Defaults to the second shape which is d6

	dice.die_shape[0] = 4;
	dice.die_shape[1] = 6;
	dice.die_shape[2] = 8;
	dice.die_shape[3] = 10;
	dice.die_shape[4] = 12;
	dice.die_shape[5] = 20;
}

uint8_t dice_shape_value() {
	return dice.die_shape[dice.shape_index];
}

void change_dice_shape() {
	dice.shape_index = (dice.shape_index + 1) % 6;
	_clear_dice_array(); // All dice in the buffer must be of the same shape
}

uint8_t roll_dice_once() {
	uint8_t roll_result = _roll();
	_clear_dice_array();
	dice.die[dice.digit_index] = roll_result;
	return roll_result;
}

uint8_t roll_dice_append() {
	uint8_t roll_result = _roll();

	if (dice.digit_index == 0) {
		// The digit index wrapped around back to 0 implies that the buffer is full
		// so let's clear the buffer
		_clear_dice_array();
	}

	dice.die[dice.digit_index] = roll_result;

	if (dice_shape_value() < 10) {
		// The buffer hold up to four single digit dice
		dice.digit_index = (dice.digit_index + 1) % 4;
	} else {
		// Otherwise it hold up to two double digit dice
		dice.digit_index = (dice.digit_index + 2) % 4;
	}

	return roll_result;
}

void roll_all_dice() {
	_clear_dice_array();
	uint8_t d_i = (dice_shape_value() < 10) ? 1 : 2;
	for (uint8_t i = 0; i < 4; i += d_i) {
		dice.die[i] = _roll();
	}
}

void get_dice_values(uint8_t *dice_vals) {
	for (uint8_t i = 0; i < 4; i++) {
		dice_vals[i] = dice.die[i];
	}
}

static uint8_t _roll() {
	// Get a 16-bit random value then map it into a range from 1 to 
	// the dice shape value (inclusive).
	// The 16-bit uint can be safely truncated to a 8-bit uint as it'll never
	// go above 20 or even 255.
	return rnd_xorshift() % dice_shape_value() + 1;
}

static void _clear_dice_array() {
	for (uint8_t i = 0; i < 4; i++) {
		dice.die[i] = 0;
	}

	dice.digit_index = 0;
}
