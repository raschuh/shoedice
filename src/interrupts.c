/*
 * interrupts.c
 *
 *  Created on: June 10, 2020
 *      Author: raschuh
 * 
 * All ISR implementations goes here.
 */

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "globals.h"

volatile uint16_t millis_clock = 0;
volatile uint8_t mode_btn_register = 0xFF;
volatile uint8_t roll_btn_register = 0xFF;

/*
    Timer0 Compare Match A Interrupt

    Timer0 is set up to generate a tick per 1 millisecond. It is used to
    clock the system millisecond clock and debounce the push buttons.
*/
ISR(TIM0_COMPA_vect) {
    static volatile uint8_t debounce_counter = 0;

	millis_clock++;
    debounce_counter++;

    if (debounce_counter > 16) {
        debounce_counter = 0;

        uint8_t pina = PINA;

        mode_btn_register = (mode_btn_register << 1) | ((pina & _BV(MODE_BTN_PIN)) >> MODE_BTN_PIN);
        roll_btn_register = (roll_btn_register << 1) | ((pina & _BV(ROLL_BTN_PIN)) >> ROLL_BTN_PIN);        
    }
}

/*
    PCINT0 - Pin Change Interrupt on PORTA

    Wake up the system if any one of the buttons is pressed.

    This ISR does nothing -- it's only there so AVR can vectored it properly.
*/
ISR(PCINT0_vect) {

}
